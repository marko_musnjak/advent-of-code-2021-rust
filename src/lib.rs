extern crate aoc_runner;

#[macro_use]
extern crate aoc_runner_derive;
extern crate crypto;
extern crate regex;
extern crate itertools;
extern crate indoc;

pub mod day01;
pub mod day02;

aoc_lib!{ year = 2021 }
