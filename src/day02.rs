use std::str::FromStr;

#[derive(Debug, PartialEq)]
pub enum Action {
    Forward,
    Down,
    Up,
}

impl FromStr for Action {
    type Err = ();

    fn from_str(input: &str) -> Result<Action, Self::Err> {
        match input {
            "forward" => Ok(Action::Forward),
            "down" => Ok(Action::Down),
            "up" => Ok(Action::Up),
            _ => Err(()),
        }
    }
}

pub struct Command {
    action: Action,
    count: usize,
}

pub struct SubmarineState {
    distance: usize,
    depth: usize,
    aim: usize,
}

#[aoc_generator(day2)]
pub fn input_generator(input: &str) -> Vec<Command> {
    input
        .lines()
        .map(|l| {
            let parts = l.split(" ").collect::<Vec<_>>();
            Command {
                action: Action::from_str(parts[0]).unwrap(),
                count: parts[1].parse::<usize>().unwrap(),
            }
        })
        .collect()
}

#[aoc(day2, part1)]
pub fn part1(input: &Vec<Command>) -> usize {
    let starting_state = SubmarineState {
        distance: 0,
        depth: 0,
        aim: 0,
    };
    let final_state = input
        .iter()
        .fold(starting_state, |state, cmd| match cmd.action {
            Action::Forward => SubmarineState {
                distance: state.distance + cmd.count,
                depth: state.depth,
                aim: state.aim,
            },
            Action::Down => SubmarineState {
                distance: state.distance,
                depth: state.depth + cmd.count,
                aim: state.aim,
            },
            Action::Up => SubmarineState {
                distance: state.distance,
                depth: state.depth - cmd.count,
                aim: state.aim,
            },
        });
    final_state.distance * final_state.depth
}

#[aoc(day2, part2)]
pub fn part2(input: &Vec<Command>) -> usize {
    let starting_state = SubmarineState {
        distance: 0,
        depth: 0,
        aim: 0,
    };
    let final_state = input
        .iter()
        .fold(starting_state, |state, cmd| match cmd.action {
            Action::Forward => SubmarineState {
                distance: state.distance + cmd.count,
                depth: state.depth + state.aim * cmd.count,
                aim: state.aim,
            },
            Action::Down => SubmarineState {
                distance: state.distance,
                depth: state.depth,
                aim: state.aim + cmd.count,
            },
            Action::Up => SubmarineState {
                distance: state.distance,
                depth: state.depth,
                aim: state.aim - cmd.count,
            },
        });
    final_state.distance * final_state.depth
}

#[cfg(test)]
mod tests {
    use super::{input_generator, part1, part2};
    use indoc::indoc;

    static INPUT: &str = indoc! {"
        forward 5
        down 5
        forward 8
        up 3
        down 8
        forward 2
        "};

    #[test]
    fn sample_part1() {
        let processed_input = input_generator(INPUT);
        assert_eq!(part1(&processed_input), 150);
    }

    #[test]
    fn sample_part2() {
        let processed_input = input_generator(INPUT);
        assert_eq!(part2(&processed_input), 900);
    }
}
