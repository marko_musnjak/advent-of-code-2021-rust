use itertools::Itertools;

#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<usize> {
    input.lines().map(|l| l.trim().parse().unwrap()).collect()
}

#[aoc(day1, part1)]
pub fn part1(input: &Vec<usize>) -> usize {
    input.windows(2).filter(|a| a[0] < a[1]).count()
}

#[aoc(day1, part2)]
pub fn part2(input: &Vec<usize>) -> usize {
    input
        .windows(3)
        .map(|s| s.iter().sum::<usize>())
        .tuple_windows()
        .filter(|(a, b)| a < b)
        .count()
}

#[cfg(test)]
mod tests {
    use indoc::indoc;
    use super::{input_generator, part1, part2};

    static INPUT: &str =indoc! {"
        199
        200
        208
        210
        200
        207
        240
        269
        260
        263
        "};

    #[test]
    fn sample_part1() {
        let processed_input = input_generator(INPUT);
        assert_eq!(part1(&processed_input), 7);
    }

    #[test]
    fn sample_part2() {
        let processed_input = input_generator(INPUT);
        assert_eq!(part2(&processed_input), 5);
    }
}
