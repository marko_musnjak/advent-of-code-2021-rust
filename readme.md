# Advent of Code 2021 in Rust

Run `cargo aoc` to run the latest day

Run `cargo test` to run all tests

Based on [cargo-aoc](https://github.com/gobanos/cargo-aoc)
